package carbon

import (
	"encoding/json"
	"fmt"
	"log"
	"testing"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func connect() *gorm.DB {
	dsn := "gouguoyin:JZfuc2@QZ1D14G@tcp(pc-2zejx3n634p6ekew4.rwlb.rds.aliyuncs.com:3306)/capital_push_dev?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	fmt.Println("连接成功")
	return db
}

func TestBD_Select(t *testing.T) {
	db := connect()

	type Demo struct {
		ID       int64  `json:"id"`
		Name     string `json:"name"`
		Age      int    `json:"age"`
		Birthday Carbon `json:"birthday"`
	}

	var users []*Demo

	db.Find(&users, []int{45, 46})

	for _, user := range users {
		user.Birthday = user.Birthday.SetLayout(DateLayout)
	}

	data, err := json.Marshal(&users)
	if err != nil {
		// Error handle...
		log.Fatal(err)
	}
	fmt.Printf("%s", data)

	// db.Model(&person).Update("birthday", Now().SubYears(18).SetLayout(DateLayout))
}
