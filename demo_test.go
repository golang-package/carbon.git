package carbon

import (
	"fmt"
	"os"
	"testing"
)

func TestDemo1(t *testing.T) {
	// type Student2 struct {
	// 	Birthday1 Carbon `json:"birthday1"`
	// 	Birthday2 Carbon `json:"birthday2"`
	// }

	// tag := NewTag()
	// tag.SetLayout(RFC3339Layout)
	// tag.SetTimezone(PRC)
	// now := SetTag(tag).Parse("2020-08-05 13:14:15", PRC)

	// student := Student2{
	// 	Birthday1: now,
	// 	Birthday2: now,
	// }

	// data, marshalErr := json.Marshal(student)
	// fmt.Println("marshal error:", marshalErr)
	// // assert.NotNil(t, marshalErr)
	// fmt.Println(string(data))

	// tag := NewTag()
	// tag.SetLayout(RFC3339Layout)
	// c1 := NewCarbon().SetTag(tag)
	// str := `{"birthday1":"2020-08-05T13:14:15+08:00","birthday2":"2020-08-05T13:14:15+08:00"}`
	// person2 := Student2{
	// 	Birthday1: c1,
	// 	Birthday2: c1,
	// }

	// var person2 Student2
	// tag := NewTag()
	// tag.SetLayout(RFC3339Layout)
	// // c1 := NewCarbon().SetTag(tag)
	// person2.Birthday1.SetTag(tag)
	// person2.Birthday2.SetTag(tag)
	//
	// unmarshalErr := json.Unmarshal([]byte(str), &person2)
	// if unmarshalErr != nil {
	// 	// 错误处理
	// 	log.Fatal(unmarshalErr)
	// }
	// fmt.Printf("Birthday1:%s\n", person2.Birthday1)
	// fmt.Printf("Birthday2:%s\n", person2.Birthday2)

}

func TestDemo2(t *testing.T) {
	// rawTime := Parse("2022-05-01", UTC)
	// go say(rawTime)
	// go say(rawTime)
	// go say(rawTime)
	// time.Sleep(100 * time.Millisecond)
	// type Student1 struct {
	// 	Birthday1 Carbon `json:"birthday1" tz:""`
	// 	Birthday2 Carbon `json:"birthday2"`
	// }
	//
	// student := Student1{
	// 	Birthday1: c.Parse("2020-08-05 13:14:15"),
	// 	Birthday2: c.Parse("2020-08-05 13:14:15"),
	// }
	//
	// err := LoadTag(&student)
	// if err != nil {
	// 	panic(err.Error())
	// }
	//
	// data, marshalErr := json.Marshal(student)
	// fmt.Println("marshal error:", marshalErr)
	// // assert.NotNil(t, marshalErr)
	// fmt.Println(string(data))
}

func say(rawTime Carbon) {
	formatted := rawTime.Lunar().ToDateString()
	fmt.Println(formatted)
}

func TestDemo3(t *testing.T) {
	// log.Println(`===========================`)
	// log.Println(`Carbon is fails:`)
	//
	// tt := `0001-01-01 00:00:00 +0000 GMT`
	// log.Println(`Time: `, t)
	//
	// parsed := Parse(tt).ToDateTimeString()
	// log.Println(`Parsed: `, parsed)
	//
	// valid := Parse(tt).IsValid()
	// log.Println(`Valid: `, valid)
	//
	// log.Println(`===========================`)
	//
	// log.Println(`Carbon is succeeds:`)
	//
	// tt = `0002-01-01 00:00:00 +0000 GMT`
	// log.Println(`Time: `, t)
	//
	// parsed = Parse(tt).ToDateTimeString()
	// log.Println(`Parsed: `, parsed)
	//
	// valid = Parse(tt).IsValid()
	// log.Println(`Valid: `, valid)

}

func TestDemo4(t *testing.T) {
	// x := ParseByLayout("10:40AM", time.Kitchen)
	// fmt.Println(x.Layout(time.Kitchen))

	r1 := Parse("0000-00-00 00:00:00")
	fmt.Println("r", r1.IsZero())

	// SetDefault(Default{
	// 	Layout:       DateTimeLayout,
	// 	Timezone:     PRC,
	// 	WeekStartsAt: Monday,
	// 	Locale:       "zh-CN",
	// })
	//
	// type YHXDayUp struct {
	// 	ID        int32  `gorm:"column:id;primaryKey;autoIncrement:true" json:"id"`
	// 	DeviceId  int32  `json:"device_id" comment:"设备id"`
	// 	CreatedAt Carbon `gorm:"column:created_at;type:timestamp" json:"created_at" comment:"创建时间"`
	// 	UpdatedAt Carbon `gorm:"column:updated_at;type:timestamp" json:"updated_at"`
	// }
	//
	// info := YHXDayUp{}
	// info.CreatedAt = Now()
	//
	// fmt.Println("dd", info.CreatedAt.ToString())
}

func TestDemo5(t *testing.T) {
	// r := Parse("2016-03-20 00:00:00").Persian()
	// fmt.Println(r.String())

	r := Parse("2020-08-05 13:14:15").Persian().ToShortWeekString()
	fmt.Println("r", r)

	// p := CreateFromPersian(622, 1, 1, 0, 0, 0)
	// fmt.Println(p.String())
	// SetDefault(Default{
	// 	Layout:       DateTimeLayout,
	// 	Timezone:     Local,
	// 	WeekStartsAt: Monday,
	// 	Locale:       "zh-CN",
	// })
	// type Project struct {
	// 	StartDate DateTime `gorm:"column:start_date" json:"startDate"`
	// 	EndDate   DateTime `gorm:"column:end_date" json:"endDate"`
	// }
	//
	// person := new(Project)
	// json := `{"startDate":"2024-10-01 00:00:00","endDate":"2024-10-31 23:59:59"}`
	// err := json2.Unmarshal([]byte(json), &person)
	// if err != nil {
	// 	// 错误处理
	// 	log.Fatal(err)
	// }
	//
	// fmt.Println(person.StartDate.Location())

}

func TestDemo6(t *testing.T) {
	r := os.Getenv("TZ")
	fmt.Println("r", r)

}
