// Package jewish is part of the carbon package.
package jewish

import (
	"gitee.com/golang-package/carbon/v2/calendar"
)

const (
	MinYear = 1
	MaxYear = 9999
)

var hebrewMonthMap = map[int]string{
	1:  "Nissan", // 'Nisan' in HebCal
	2:  "Iyar",   // 'Iyyar' in HebCal
	3:  "Sivan",
	4:  "Tammuz", // 'Tamuz' in HebCal
	5:  "Av",
	6:  "Elul",
	7:  "Tishrei",
	8:  "Cheshvan",
	9:  "Kislev",
	10: "Teves",  // 'Tevet' in HebCal
	11: "Shevat", // "Sh'vat" in HebCal
	12: "Adar",
	13: "Adar II",
}

// Gregorian defines a Gregorian struct.
// 定义 Gregorian 结构体
type Gregorian struct {
	calendar.Gregorian
}

// Jewish defines a Jewish struct.
// 定义 Jewish 结构体
type Jewish struct {
	year, month, day, hour, minute, second int
	afterSunset                            bool
	Error                                  error
}

// IsLeapYear reports whether is a leap year.
// 是否是闰年
func (j Jewish) IsLeapYear(year int) bool {
	yearIndex := year % 19
	return yearIndex == 0 ||
		yearIndex == 3 ||
		yearIndex == 6 ||
		yearIndex == 8 ||
		yearIndex == 11 ||
		yearIndex == 14 ||
		yearIndex == 17
}
